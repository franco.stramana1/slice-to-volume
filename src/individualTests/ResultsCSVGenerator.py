import os
import Config
from individualTests.ResultsManager import IndividualResultsManager
from datasets.HeartDataSet import HeartDataSet

numberOfSeries = 10
numberOfImages = 10
numberOfInitials = 3
dataset = HeartDataSet()

def concatResults(resultsManager,gt):
    record = ""
    record += str(gt.GetParameters()).replace("(","").replace(")","")     
    record += "," + str(resultsManager.getInitialTransformation())
    record += "," + str(resultsManager.initialSAD)
    record += "," + str(resultsManager.initialSSD)
    record += "," + str(resultsManager.getContinuousSolution())
    record += "," + str(resultsManager.continuousSAD)
    record += "," + str(resultsManager.continuousSSD)
    record += "," + str(resultsManager.getDiscreteSolution())
    record += "," + str(resultsManager.discreteSAD)
    record += "," + str(resultsManager.discreteSSD)
    record += "," + str(resultsManager.getRefinedSolution())
    record += "," + str(resultsManager.refinedSAD)
    record += "," + str(resultsManager.refinedSSD)
    return record.replace("[", "").replace("]", "").replace(" ", "")
    

directory = Config.INDIVIDUAL_TEST_PATH
if os.path.exists(directory):
    with open(directory + "/results.csv",'w+') as file: 
        for nserie in xrange(numberOfSeries):
            for nslice in xrange(numberOfImages):
                for index in xrange(numberOfInitials):
                    resultsManager = IndividualResultsManager()
                    if resultsManager.parse(nserie, nslice, index):
                        gt = dataset.getGoalSliceGroundTruth(nserie,nslice)
                        record = concatResults(resultsManager,gt)
                        file.write(record+"\n")
                    else:
                        print "NO file: " + str(nserie) + " " + str(nslice) + " " + str(index)
                        
