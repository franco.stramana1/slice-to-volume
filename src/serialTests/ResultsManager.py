import Config
import os
import os.path

class SerialResultsManager(object):

    def __init__(self):
        
        self.initialTransformation = {    
            'RotateX': 0,
            'RotateY': 0,
            'RotateZ': 0,
            'TranslateX': 0,
            'TranslateY': 0,
            'TranslateZ': 0
        }
        self.initialSAD = -1
        self.initialSSD = -1
        
        self.solution = {    
            'RotateX': 0,
            'RotateY': 0,
            'RotateZ': 0,
            'TranslateX': 0,
            'TranslateY': 0,
            'TranslateZ': 0
        }
        self.solutionSAD = -1
        self.solutionSSD = -1

    def setInitialTransformation(self, parameters):
        self.initialTransformation['RotateX'] = parameters[0] 
        self.initialTransformation['RotateY'] = parameters[1]
        self.initialTransformation['RotateZ'] = parameters[2]
        self.initialTransformation['TranslateX'] = parameters[3]
        self.initialTransformation['TranslateY'] = parameters[4]
        self.initialTransformation['TranslateZ'] = parameters[5]
        
    def getInitialTransformation(self):
        transformation = []        
        transformation.append(self.initialTransformation['RotateX'])
        transformation.append(self.initialTransformation['RotateY'])
        transformation.append(self.initialTransformation['RotateZ'])
        transformation.append(self.initialTransformation['TranslateX'])
        transformation.append(self.initialTransformation['TranslateY'])
        transformation.append(self.initialTransformation['TranslateZ'])
        return transformation
        
    def setSolution(self, parameters):
        self.solution['RotateX'] = parameters[0] 
        self.solution['RotateY'] = parameters[1]
        self.solution['RotateZ'] = parameters[2]
        self.solution['TranslateX'] = parameters[3]
        self.solution['TranslateY'] = parameters[4]
        self.solution['TranslateZ'] = parameters[5]
        
    def getSolution(self):
        transformation = []        
        transformation.append(self.solution['RotateX'])
        transformation.append(self.solution['RotateY'])
        transformation.append(self.solution['RotateZ'])
        transformation.append(self.solution['TranslateX'])
        transformation.append(self.solution['TranslateY'])
        transformation.append(self.solution['TranslateZ'])
        return transformation

    def parse(self,nserie,nslice,method):
        #TODO: mejorar este parse
        directory = Config.SERIAL_TEST_PATH + "/" + str(method) + "/" + str(nserie)
        path = directory + "/" + "image_" + str(nslice + 1)
        if not os.path.exists(directory) or not os.path.isfile(path):
            return False
        with open(path,'r') as file: 
            for line in file:
            
                if line.startswith("initial_RotateX"):
                    self.initialTransformation['RotateX'] = float(line.replace("initial_RotateX = ", ""))
                if line.startswith("initial_RotateY"):
                   self.initialTransformation['RotateY'] = float(line.replace("initial_RotateY = ", ""))
                if line.startswith("initial_RotateZ"):
                   self.initialTransformation['RotateZ'] = float(line.replace("initial_RotateZ = ", ""))
                if line.startswith("initial_TranslateX"):
                   self.initialTransformation['TranslateX'] = float(line.replace("initial_TranslateX = ", ""))
                if line.startswith("initial_TranslateY"):
                    self.initialTransformation['TranslateY'] = float(line.replace("initial_TranslateY = ", ""))
                if line.startswith("initial_TranslateZ"):
                    self.initialTransformation['TranslateZ']= float(line.replace("initial_TranslateZ = ", ""))
                if line.startswith("initial_SAD"):
                    self.initialSAD = float(line.replace("initial_SAD = ", ""))
                if line.startswith("initial_SSD"):
                    self.initialSSD = float(line.replace("initial_SSD = ", ""))
                    
                if line.startswith("solution_RotateX"):
                    self.solution['RotateX'] = float(line.replace("solution_RotateX = ", ""))
                if line.startswith("solution_RotateY"):
                   self.solution['RotateY'] = float(line.replace("solution_RotateY = ", ""))
                if line.startswith("solution_RotateZ"):
                   self.solution['RotateZ'] = float(line.replace("solution_RotateZ = ", ""))
                if line.startswith("solution_TranslateX"):
                   self.solution['TranslateX'] = float(line.replace("solution_TranslateX = ", ""))
                if line.startswith("solution_TranslateY"):
                    self.solution['TranslateY'] = float(line.replace("solution_TranslateY = ", ""))
                if line.startswith("solution_TranslateZ"):
                    self.solution['TranslateZ']= float(line.replace("solution_TranslateZ = ", ""))
                if line.startswith("solution_SAD"):
                    self.solutionSAD = float(line.replace("solution_SAD = ", ""))
                if line.startswith("solution_SSD"):
                    self.solutionSSD = float(line.replace("solution_SSD = ", ""))
                        
        return True                    
                    
    def save(self, nserie, nslice, method):
        directory = Config.SERIAL_TEST_PATH + "/" + str(method) + "/" + str(nserie)
        if not os.path.exists(directory):
            os.makedirs(directory)
        with open(directory + "/" + "image_" + str(nslice + 1),'a') as file: 
            file.write("TRANSFORMACION INICIAL:\n")
            for key,val in self.initialTransformation.iteritems():
                file.write("initial_"+key+" = "+str(val)+"\n")
            file.write("initial_SAD = "+str(self.initialSAD)+"\n")
            file.write("initial_SSD = "+str(self.initialSSD)+"\n")
            file.write("\n")
            file.write("SOLUCION:\n")
            for key,val in self.solution.iteritems():
                file.write("solution_"+key+" = "+str(val)+"\n")
            file.write("solution_SAD = "+str(self.solutionSAD)+"\n")
            file.write("solution_SSD = "+str(self.solutionSSD)+"\n")
            file.write("\n")